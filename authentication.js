﻿'use strict';
var http = require('http');
var soap = require('soap');
var q = require('q');

// enumerations
var AccountType = { 'None_Selected': 0, 'Client': 1, 'Informational': 2, 'Marketing_Group': 3, 'Other': 4, 'Prospect': 5 };
var AccountClassificationType = { None_Selected: 0, Agency: 1, Agent: 2, Group: 3, Individual: 4, Marketing_Group: 5 };

var sessionID = "";
var currentUser;
var accounts;
var user = '';
var sessionInfo;
var loginClient;

var User = function (sessionID, currentUser, accounts) {
    this.sessionID = 'sessionID';
    this.currentUser = 'currentUser';
    this.accounts = 'account';
    this.url = 'https://';
};

//sessionID is a string
User.prototype.setSessionID = function(sessionID) {
    this.sessionID = sessionID;
};

//currentUser is object with multible values
User.prototype.setCurrentUser = function (currentUser) {
    this.currentUser = currentUser;
};

//acccounts is an array of objects with mutiple values
User.prototype.setAccounts = function (accounts) {
    this.accounts = accounts;
};

User.prototype.setUrl = function (url) {
    this.url = url;
};



module.exports = function doLogin(urlLoginV2, username, password) {
    getSessionID(urlLoginV2, username, password)
        .then(getCurrentUser)
        .then(getAccounts);
    //.then (getAccounts(sessionID, urlLoginV2))
    



};


       

function getSessionID(urlLoginV2, username, password) {
    var defer = q.defer();
    
    let args = {
        username: username,
        password: password
    };


    return soap.createClientAsync(urlLoginV2).then((client) => {
        loginClient = client;
        return loginClient.loginAsync(args);

    }).then((result) => {
        console.log(result);
        return q.all(urlLoginV2, result.result.sessionID);
    });
}



    //});



function getCurrentUser(urlLoginV2, sessionID) {
    
    // setup args for currentUser
    let args = {
        sessionID: sessionID
    };

    return loginClient.getCurrentUserAsync(args)
        .then((result) => {
            console.log(result);
            currentUser = result;
            return q.all(urlLoginV2, result.result.sessionID);
        });
}
    //return loginClient.getCurrentUserAsync(args, function (err, result) {
    //    if (err) defer.reject(err);
    //    else {
    //        currentUser = result;

    //        //console.log(defer.promise);
    //        console.log(currentUser);
    //        return currentUser;
    //    }
    //});





function getAccounts(urlLoginV2, sessionID) {
    return new Promise(function (resolve, reject) {
        const urlBrokerConnectV4 = urlLoginV2.replace('LoginV2', 'BrokerConnectV4');

        soap.createClient(urlBrokerConnectV4, function (err, client) {

            //each call to the brokerConnectV4 client has to have a custom header with the sessionID in it.
            client.addHttpHeader('sessionID', sessionID);

            //this will back active group and individual accounts/prospects that the user has permission to update
            let args = {
                teamMemberID: currentUser.userID,
                teamMemberIDSpecified: true,
                accountTypes: [AccountType.Client, AccountType.Prospect],
                accountClassifications: [AccountClassificationType.Group, AccountClassificationType.Individual],
                accountStatus: true
            };

            client.findAccounts(args, function (err, result) {
                if (err) reject(err);
                else resolve(result);

            });
        });
    });
}
